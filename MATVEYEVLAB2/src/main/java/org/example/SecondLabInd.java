package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.time.Duration;

public class SecondLabInd {
    private WebDriver chromeDriver;

    private static final String baseUrl = "https://prom.ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod(alwaysRun = true)
    public void preconditions(){
        chromeDriver.get(baseUrl);
    }

    //Завдання 1. Натискаємо на елемент (кнопка).
    @Test(alwaysRun = true)
    public void testButton(){
        WebElement clickButton = chromeDriver.findElement(By.linkText("Створити магазин"));
        clickButton.click();
    }
    //Завдання 2. Робимо пошуковий запит.
    @Test(alwaysRun = true)
    public void testSearching(){
        WebElement searchField = chromeDriver.findElement(By.name("search_term"));
        Assert.assertNotNull(searchField);
        String inputValue = "генератор";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getAttribute("value"),inputValue);
        searchField.sendKeys(Keys.ENTER);
    }

    //Завдвння 3. Пошук кнопки через xpath.
    @Test(alwaysRun = true)
    public void testFindByXPath(){
        WebElement Xpath = chromeDriver.findElement(By.xpath( "/html/body/div[1]/div/div/div/header/div[1]/div/div[3]/div/div[1]/button"));
        Assert.assertNotNull(Xpath);
        Xpath.click();
    }

    //Завдання 4. Перевірка запиту на порожність
    @Test(alwaysRun = true)
    public void testCheckSearch(){
        WebElement searchField = chromeDriver.findElement(By.name("search_term"));
        Assert.assertNotNull(searchField);
        String inputValue = "генератор";

        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getAttribute("value"),inputValue);

        String search = searchField.getAttribute("value");

        if (search.length() == 0)
            System.out.println("Порожній запит!");
        else
            System.out.println("Все добре");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
    chromeDriver.quit();
    }
}
